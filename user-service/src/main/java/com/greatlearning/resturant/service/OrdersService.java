package com.greatlearning.resturant.service;

import com.greatlearning.resturant.entity.BillDetailsDtoList;
import com.greatlearning.resturant.entity.InventoryDetails;
import com.greatlearning.resturant.entity.TotalSalesDto;
import com.greatlearning.resturant.entity.sales.SalesResponseList;

import java.util.Optional;

public interface OrdersService {

    BillDetailsDtoList generateTodayBills();

    TotalSalesDto getTotalSalesForThisMonth();

    void saveOrder(Integer selectedItem, Optional<InventoryDetails> item);

    SalesResponseList getMaxSalesInTheMonth();

    SalesResponseList getMonthlySalesInLastYear();
}
