package com.greatlearning.resturant.service;

import com.greatlearning.resturant.entity.UserDto;
import com.greatlearning.resturant.entity.Users;

public interface UserService {

    Users createUser(UserDto userDto);

    Users getUser(String userName);

    void deleteUser(String userName);

    Users updateUser(String userName, UserDto userDto);

    Users authenticateUser(UserDto userDto);
}
