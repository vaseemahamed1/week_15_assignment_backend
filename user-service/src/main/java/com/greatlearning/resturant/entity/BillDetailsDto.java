package com.greatlearning.resturant.entity;

import java.util.List;


public class BillDetailsDto {

    private List<String> itemsOrdered;
    private Integer totalBill;

    public BillDetailsDto(List<String> itemsOrdered, Integer totalBill) {
        this.itemsOrdered = itemsOrdered;
        this.totalBill = totalBill;
    }

    public List<String> getItemsOrdered() {
        return itemsOrdered;
    }

    public void setItemsOrdered(List<String> itemsOrdered) {
        this.itemsOrdered = itemsOrdered;
    }

    public Integer getTotalBill() {
        return totalBill;
    }

    public void setTotalBill(Integer totalBill) {
        this.totalBill = totalBill;
    }

    @Override
    public String toString() {
        return "BillDetailsDto{" +
                ", itemsOrdered=" + itemsOrdered +
                ", totalBill=" + totalBill +
                '}';
    }
}
