package com.greatlearning.resturant.entity;

public class UserDto {
    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public UserDto(String username, String password, String city) {
        super();
        this.username = username;
        this.password = password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "UserDto{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
