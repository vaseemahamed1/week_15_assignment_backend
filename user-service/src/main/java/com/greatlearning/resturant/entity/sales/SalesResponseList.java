package com.greatlearning.resturant.entity.sales;

import java.util.ArrayList;
import java.util.List;

public class SalesResponseList {
    private List<SalesResponseDto> salesResponseDtoList;

    public SalesResponseList() {
        this.salesResponseDtoList = new ArrayList<>();
    }

    public List<SalesResponseDto> getSalesResponseDtoList() {
        return salesResponseDtoList;
    }

    public void setSalesResponseDtoList(List<SalesResponseDto> salesResponseDtoList) {
        this.salesResponseDtoList = salesResponseDtoList;
    }
}
