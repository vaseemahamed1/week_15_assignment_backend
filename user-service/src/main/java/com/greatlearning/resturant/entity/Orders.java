package com.greatlearning.resturant.entity;

import javax.persistence.*;
import java.sql.Date;

@Entity
public class Orders {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "item_code")
    private Integer itemCode;

    @Column(name = "order_date")
    private Date orderDate;

    @Column(name = "item_name")
    private String itemName;

    @Column(name = "price")
    private Integer price;

    public Orders() {
    }

    public Orders(Integer itemCode, Date orderDate, String itemName, Integer price) {
        this.itemCode = itemCode;
        this.orderDate = orderDate;
        this.itemName = itemName;
        this.price = price;
    }

    public Integer getItemCode() {
        return itemCode;
    }

    public void setItemCode(Integer itemCode) {
        this.itemCode = itemCode;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Orders{" +
                "id=" + id +
                ", itemCode=" + itemCode +
                ", orderDate=" + orderDate +
                ", itemName='" + itemName + '\'' +
                ", price=" + price +
                '}';
    }
}
