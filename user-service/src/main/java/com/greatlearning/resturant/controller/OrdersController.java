package com.greatlearning.resturant.controller;

import com.greatlearning.resturant.entity.*;
import com.greatlearning.resturant.repository.InventoryDetailsRepository;
import com.greatlearning.resturant.service.OrdersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/orders")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class OrdersController {

    @Autowired
    InventoryDetailsRepository inventoryDetailsRepository;

    @Autowired
    OrdersService ordersService;

    @GetMapping("/viewMenuItems")
    public InventoryDetailsList getMenuItems() {
        InventoryDetailsList response = new InventoryDetailsList();
        response.setInventoryDetailsList(inventoryDetailsRepository.findAll());
        return response;
    }

    @PostMapping("/selectItems")
    public String selectMenuItems(@RequestParam List<Integer> menuItemsIds) {
        // map menu items
        menuItemsIds.stream()
                .forEach(selectedItem -> {
                    Optional<InventoryDetails> item = inventoryDetailsRepository.findById(selectedItem);
                    if (item.isPresent()) {
                        ordersService.saveOrder(selectedItem, item);
                    }
                });
        return "order added";
    }
}
