package com.greatlearning.resturant.controller;

import com.greatlearning.resturant.entity.BillDetailsDtoList;
import com.greatlearning.resturant.entity.TotalSalesDto;
import com.greatlearning.resturant.entity.sales.SalesResponseList;
import com.greatlearning.resturant.service.OrdersService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/sales")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class SalesController {

    @Autowired
    OrdersService ordersService;

    @ApiOperation(value = "This method is used to generate bill for today")
    @GetMapping("/generateTodayBills")
    public BillDetailsDtoList generateTodayBills() {
        return ordersService.generateTodayBills();
    }

    @ApiOperation(value = "This method is used to generate bill for current month", hidden = true)
    @GetMapping("/getTotalSalesForCurrentMonth")
    public TotalSalesDto getTotalSalesForMonth() {
        return ordersService.getTotalSalesForThisMonth();
    }

    @ApiOperation(value = "This method is used to get max sales in the month", hidden = true)
    @GetMapping("/getMaxSalesInTheMonth")
    public SalesResponseList getMaxSalesInTheMonth() {
        return ordersService.getMaxSalesInTheMonth();
    }

    @ApiOperation(value = "This method is used to get monthly sales in last year", hidden = true)
    @GetMapping("/getMonthlySalesInLastYear")
    public SalesResponseList getMonthlySalesInLastYear() {
        return ordersService.getMonthlySalesInLastYear();
    }

}
