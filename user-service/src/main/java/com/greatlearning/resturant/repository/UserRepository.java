package com.greatlearning.resturant.repository;

import com.greatlearning.resturant.entity.UserDto;
import com.greatlearning.resturant.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<Users, Integer> {

    Users getByUsername(String userName);

    void deleteByUsername(String userName);

    @Query(value = "select * from USERS where username = ?1 and password = ?2", nativeQuery = true)
    Users authenticateUser(String userName, String password);
}
