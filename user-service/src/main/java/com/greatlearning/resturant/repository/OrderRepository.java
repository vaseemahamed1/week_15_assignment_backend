package com.greatlearning.resturant.repository;

import com.greatlearning.resturant.entity.Orders;
import com.greatlearning.resturant.entity.sales.SalesResponseDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.sql.Date;
import java.util.List;
import java.util.Map;

public interface OrderRepository extends JpaRepository<Orders, Integer> {

    List<Orders> findAllByOrderDate(Date date);

    @Query(value = "select * from ORDERS where month(order_date)= ?1", nativeQuery = true)
    List<Orders> findAllByOrderDateMonth(Integer month);

    @Query(value = "SELECT * FROM MAX_SALES_IN_MONTH", nativeQuery = true)
    Map<String, Number>  findMaxSalesInTheMonth();

    @Query(value = "SELECT * FROM MONTHLY_SALES_IN_LAST_YEAR", nativeQuery = true)
    Map<String, Number>  findMonthlySalesInLastYear();
}
