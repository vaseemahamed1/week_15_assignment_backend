-- create view based on orders table to get order related details
CREATE
OR REPLACE VIEW ORDERS_VIEW as
SELECT *
FROM ORDERS;


-- view to get max sales in the month
CREATE
OR REPLACE VIEW MAX_SALES_IN_MONTH AS
SELECT MONTH (ORDER_DATE) as CRITERIA, MAX (PRICE) as SALES
FROM ORDERS
GROUP BY MONTH (ORDER_DATE);


-- view to get the monthly sales for last year
CREATE
OR REPLACE VIEW MONTHLY_SALES_IN_LAST_YEAR AS
SELECT MONTH (ORDER_DATE) as CRITERIA, SUM (PRICE) as SALES
FROM ORDERS
WHERE YEAR (ORDER_DATE) = YEAR (CURRENT_DATE) - 1
GROUP BY MONTH (ORDER_DATE);


-- Insert Data into default user details
INSERT
INTO users (id, username, password, role, enabled)
VALUES (1, 'user', 'user', 'ROLE_USER', 'TRUE');
INSERT
INTO users (id, username, password, role, enabled)
VALUES (2, 'admin', 'admin', 'ROLE_ADMIN', 'TRUE');

-- Insert Data into INVENTORY_DETAILS Table
INSERT INTO INVENTORY_DETAILS
VALUES (1, 'Chips', 200);
INSERT INTO INVENTORY_DETAILS
VALUES (2, 'Hide & Seek Biscuit', 500);
INSERT INTO INVENTORY_DETAILS
VALUES (3, 'Apple', 200);
INSERT INTO INVENTORY_DETAILS
VALUES (4, 'Banana', 300);
INSERT INTO INVENTORY_DETAILS
VALUES (5, 'Bandage', 1000);
