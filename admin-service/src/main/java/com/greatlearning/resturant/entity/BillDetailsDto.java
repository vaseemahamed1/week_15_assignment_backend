package com.greatlearning.resturant.entity;

import java.util.List;


public class BillDetailsDto {

    private String userName;
    private List<String> itemsOrdered;
    private Integer totalBill;

    public BillDetailsDto(String userName, List<String> itemsOrdered, Integer totalBill) {
        this.userName = userName;
        this.itemsOrdered = itemsOrdered;
        this.totalBill = totalBill;
    }

    public BillDetailsDto() {
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public List<String> getItemsOrdered() {
        return itemsOrdered;
    }

    public void setItemsOrdered(List<String> itemsOrdered) {
        this.itemsOrdered = itemsOrdered;
    }

    public Integer getTotalBill() {
        return totalBill;
    }

    public void setTotalBill(Integer totalBill) {
        this.totalBill = totalBill;
    }

    @Override
    public String toString() {
        return "BillDetailsDto{" +
                "userName='" + userName + '\'' +
                ", itemsOrdered=" + itemsOrdered +
                ", totalBill=" + totalBill +
                '}';
    }
}
