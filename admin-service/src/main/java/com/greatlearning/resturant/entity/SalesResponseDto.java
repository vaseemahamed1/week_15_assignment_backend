package com.greatlearning.resturant.entity;

public class SalesResponseDto {

    private String criteria;
    private Integer sales;

    public SalesResponseDto(String name, Integer totalSale) {
        this.criteria = name;
        this.sales = totalSale;
    }

    public String getCriteria() {
        return criteria;
    }

    public void setCriteria(String criteria) {
        this.criteria = criteria;
    }

    public Integer getSales() {
        return sales;
    }

    public void setSales(Integer sales) {
        this.sales = sales;
    }
}
