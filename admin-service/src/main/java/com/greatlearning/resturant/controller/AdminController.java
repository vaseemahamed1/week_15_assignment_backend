package com.greatlearning.resturant.controller;

import com.greatlearning.resturant.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    private RestTemplate template;

    @GetMapping("/viewUser/{userName}")
    public Users getUser(@PathVariable String userName) {
        String url = "http://USER-SERVICE/viewUser/" + userName;
        return template.getForObject(url, Users.class);
    }

    @PostMapping("/createUser")
    public Users createUser(@RequestBody UserDto userDto) {
        String url = "http://USER-SERVICE/createUser/";
        return template.postForObject(url, userDto, Users.class);
    }

    @PutMapping("/updateUser/{userName}")
    public String updateUser(@PathVariable String userName, @RequestBody UserDto userDto) {
        String url = "http://USER-SERVICE/updateUser/" + userName;
        template.put(url, userDto);
        return "user updated";
    }

    @DeleteMapping("/deleteUser/{userName}")
    @Transactional
    public String deleteUser(@PathVariable String userName) {
        String url = "http://USER-SERVICE/deleteUser/" + userName;
        template.delete(url);
        return "user deleted";
    }

    @GetMapping("/generateTodayBills")
    public List<BillDetailsDto> generateTodayBills() {
        String url = "http://USER-SERVICE/orders/generateTodayBills";
        try {
            BillDetailsDtoList billDetailsDtoList =
                    template.getForObject(url, BillDetailsDtoList.class);
            return billDetailsDtoList.getBillDetailsDtoList();
        } catch (Exception exception) {
            return Arrays.asList();
        }
    }

    @GetMapping("/getTotalSalesForCurrentMonth")
    public TotalSalesDto getTotalSalesForMonth() {
        try {
            String url = "http://USER-SERVICE/orders/getTotalSalesForCurrentMonth";
            return template.getForObject(url, TotalSalesDto.class);
        } catch (Exception exception) {
            return new TotalSalesDto();
        }
    }

    @GetMapping("/getMaxSalesInTheMonth")
    public SalesResponseList getMaxSalesInTheMonth() {
        String url = "http://USER-SERVICE/sales/getMaxSalesInTheMonth";
        return template.getForObject(url, SalesResponseList.class);
    }

    @GetMapping("/getMonthlySalesInLastYear")
    public SalesResponseList getMonthlySalesInLastYear() {
        String url = "http://USER-SERVICE/sales/getMonthlySalesInLastYear";
        return template.getForObject(url, SalesResponseList.class);
    }

}
