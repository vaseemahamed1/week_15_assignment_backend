-- Insert Data into default user details

INSERT INTO users (id, username, password, role, enabled)
VALUES (2, 'admin',
        '$2a$10$htVgh66BTs0Hlydyd/qMJO2DBVDRNMSCrKSKxCs4zvQuc66AGY3qa',
        'ROLE_ADMIN', 'TRUE');
